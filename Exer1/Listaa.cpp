//Librerias
#include <iostream>
using namespace std;

#include "Listaa.h"

Listaa::Listaa() {}

void Listaa::agregar(Numero *numero) {
    Nodo *tmp;

    /* crea un nodo . */ 
    tmp = new Nodo;

    /* asigna la instancia de Numero. */
    tmp->numero = numero;
    
    /* apunta a NULL por defecto. */
    tmp->sgte = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo 
    como el último de la lista. */
    } else {
        this->ultimo->sgte = tmp;
        this->ultimo = tmp;
    }
}

void Listaa::mostrar() {
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << "\n";
    while (tmp != NULL) {
        cout << "Numero: " << tmp->numero->get_numero() << endl;
        tmp = tmp->sgte;
    }
}

//Funcion para hacer dos listas
void Listaa::dos_listas() {
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;
    /* se crean listas para almacenar los numeros negativos y positivos respectivamente */
    Listaa *l1 = new Listaa();
    Listaa *l2 = new Listaa();
    /* variables para almacenar numero de tmp->numero->get_numero() en int*/
    int var1 = 0;
    int var2 = 0;

    cout << "\n";
    while (tmp != NULL) {
        // si el numero es igual a mayor a 0 lo mete a una lista (l1)
        if (tmp->numero->get_numero() >= 0){
            var1 = tmp->numero->get_numero();
            //crea objeto Numero para poder ingresarlo a la lista l1
            Numero *a = new Numero(var1);
            //Se agrega objeto numero a la lista
            l1->agregar(a);
            cout << "\n";
            l1->ordenar();

        }

         // si el numero es menor a 0 lo mete a una lista (l2)
        else if (tmp->numero->get_numero() < 0){
            var2 = tmp->numero->get_numero();
            Numero *b = new Numero(var2);
            //Se agrega el objeto numero a la lista 
            l2->agregar(b);
            
            l2->ordenar();
        }
        tmp = tmp->sgte;
    }

    cout << "Los numeros positivos son: ";
    //Se imprime la lista de los positivos (l1)
    l1->mostrar();
    cout << "\n";
    cout << "Los numeros negativos son: ";
    //Se imprime la lista de los positivos (l1)
    l2->mostrar();

}

// FUncion para ordenar listas 
void Listaa::ordenar() {
    // se crea nodo auxiliar
    Nodo *p = this->raiz;
    //Numero aux = new Numero();
    
    while(p != NULL) {
        //puntero para evaluar el nodo siguiente a p
        Nodo *j = p->sgte;
        //while para comparar nodo p con todos los nodos de la lista (dinamico)
        while(j != NULL) {
            //comparar los elementos (dato/numero) de los nodos, si la condicion se cumple, se moveran las posiciones
            if(p->numero->get_numero() > j->numero->get_numero()){
                int aux = j->numero->get_numero();
                int aux2 = p->numero->get_numero();
                //reemplazo posiciones 
                j->numero->set_numero(aux2);
                p->numero->set_numero(aux);
            }
            j = j->sgte;
        }
        p = p->sgte;
    }
}  



