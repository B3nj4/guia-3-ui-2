/* Escriba un programa que cree una lista desordenada de números y luego la divida en dos listas inde-
pendientes ordenadas ascendentemente, una formada por los números positivos y otra por los números
negativos.
*/

//Librerias
#include <iostream>
using namespace std;

//Clases
#include "Numero.h"
#include "Listaa.h"

// Menu
int menu(){
    string opc;

    cout << "----------------------------" << endl;
    cout << "Agregar número           [1]" << endl;
    cout << "Separar lista principal  [2]" << endl; 
    cout << "Salir                    [0]" << endl;
    cout << "----------------------------" << endl;
    cout << "Opcion: ";
    cin >> opc;
    
  return stoi(opc);
}

// Funcion principal del programa
int main(void) {
    
    // Objeto lista
    Listaa *lista = new Listaa();
    
    //Variables
    int opc = 0;
    int line;

    system("clear");

    //Interactuar con el menu
    do {
        opc = menu();

        switch (opc) {
            //Agregar numero y mostrar informacion
            case 1:
                cout << "Valor: ";
                cin >> line;
                lista->agregar(new Numero(line));
                lista->ordenar();
                lista->mostrar();
                break;

            //Ver los numeros positivos y negativos de la lista principal en sublistas separadas
            case 2:
                lista->dos_listas();
                break;

        }
    } while (opc != 0); // Sale del programa

    return 0;
}
