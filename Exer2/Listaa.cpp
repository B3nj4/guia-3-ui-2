//Librerias
#include <iostream>
using namespace std;

#include "Listaa.h"

Listaa::Listaa() {}

void Listaa::agregar(Nombre *nombre) {
    Nodo *tmp;

    /* crea un nodo . */ 
    tmp = new Nodo;

    /* asigna la instancia de Numero. */
    tmp->nombre = nombre;
    
    /* apunta a NULL por defecto. */
    tmp->sgte = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo 
    como el último de la lista. */
    } else {
        this->ultimo->sgte = tmp;
        this->ultimo = tmp;
    }
}

void Listaa::mostrar() {
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << "\n";
    while (tmp != NULL) {
        cout << "Nombre: " << tmp->nombre->get_nombre() << endl;
        tmp = tmp->sgte;
    }
}

void Listaa::ordenar() {
    // se crea nodo auxiliar
    Nodo *p = this->raiz;
    
    while(p != nullptr) {
        //puntero para evaluar el nodo siguiente a p
        Nodo *j = p->sgte;
        //while para comparar nodo p con todos los nodos de la lista (dinamico)
        while(j != nullptr) {
            //comparar los elementos (dato/nombre) de los nodos, si la condicion se cumple, se moveran las posiciones
            if(p->nombre->get_nombre() > j->nombre->get_nombre()){
                string aux = j->nombre->get_nombre();
                string aux2 = p->nombre->get_nombre();
                //reemplazo posiciones 
                j->nombre->set_nombre(aux2);
                p->nombre->set_nombre(aux); 
            }
            j = j->sgte;
        }
        p = p->sgte;
    }
}  
