//Librerias
#include "Nombre.h"

//Construcctor de Nombre
Nombre::Nombre(string nombre) {
    this->nombre = nombre;
}

//Metodo (obtener nombre)
string Nombre::get_nombre() {
    return this->nombre;
}

void Nombre::set_nombre(string nombre){
    this->nombre = nombre;
}
