#ifndef NOMBRE_H
#define NOMBRE_H

//Librerias
#include <iostream>
using namespace std;

class Nombre {
    private:
        string nombre = "\0";

    public:
        /* constructor */
        Nombre(string nombre);
        
        /* métodos get */
        string get_nombre();
        void set_nombre(string nombre);
};
#endif
