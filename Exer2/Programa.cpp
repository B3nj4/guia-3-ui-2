/* Escriba un programa que cree una lista ordenada con nombres (string). Por cada ingreso, muestre el
contenido de la lista.
*/

//Librerias
#include <iostream>
using namespace std;

//Clases
#include "Nombre.h"
#include "Listaa.h"

// Menu
int menu(){
    string opc;

    cout << "------------------" << endl;
    cout << "Agregar nombre [1]" << endl;
    cout << "Salir          [2]" << endl;
    cout << "------------------" << endl;
    cout << "Opcion: ";
    cin >> opc;
    
  return stoi(opc);
}

// Funcion principal del programa
int main(void) {
    
    // Objeto lista
    Listaa *lista = new Listaa();
    
    //Variables
    int opc = 0;
    string line;

    system("clear");

    //Interactuar con el menu
    do {
        opc = menu();

        switch (opc) {
            //Agregar nombre y mostrar informacion
            case 1:
                cout << "Ingrese algún nombre: ";
                cin >> line;
                lista->agregar(new Nombre(line));
                lista->ordenar();
                lista->mostrar();
                break;
        }
    } while (opc != 2); // Sale del programa

    return 0;
}
