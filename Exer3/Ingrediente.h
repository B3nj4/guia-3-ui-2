#ifndef INGREDIENTE_H
#define INGREDIENTE_H

//Librerias
#include <iostream>
using namespace std;

class Ingrediente {
    private:
        string ingrediente = "\0";

    public:
        /* constructor */
        Ingrediente(string ingrediente);
        
        /* métodos get */
        string get_ingrediente();
};
#endif
