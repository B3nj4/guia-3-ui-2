//Librerias
#include <iostream>
using namespace std;

#include "ListaPostre.h"
#include "Listaingrediente.h"
#include "Postre.h"
#include "Ingrediente.h"

ListaPostre::ListaPostre() {}

void ListaPostre::agregar_postre(Postre *postre) {
    NodoPostre *tmp;

    /* crea un nodo . */ 
    tmp = new NodoPostre;

    /* asigna la instancia de Numero. */
    tmp->postre = postre;
    
    /* apunta a NULL por defecto. */
    tmp->sgte = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo 
    como el último de la lista. */
    } else {
        this->ultimo->sgte = tmp;
        this->ultimo = tmp;
    }
}

void ListaPostre::mostrar_postres() {
    /* utiliza variable temporal para recorrer la lista. */
    NodoPostre *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << "\n";
    while (tmp != NULL) {
        cout << "Postre: " << tmp->postre->get_postre() << endl;
        tmp = tmp->sgte;
    }
}

void ListaPostre::eliminar_postre(string dato){
    NodoPostre *p = this->raiz;

    if(p->postre->get_postre() == dato) {
        raiz = raiz->sgte;
        delete(p);
    }
    
    else{
        while(p->postre->get_postre() != dato){
            p = p->sgte;
        }

        NodoPostre *j = this->raiz; 

        while(j->sgte != p){
            j = j->sgte;
        }
        
        j->sgte = p->sgte;

        p->sgte = NULL;
        delete(p);
    }
}

void ListaPostre::ordenar_postres(){
     // se crea nodo auxiliar
    NodoPostre *p = this->raiz;
    
    while(p != nullptr) {
        //puntero para evaluar el nodo siguiente a p
        NodoPostre *j = p->sgte;
        //while para comparar nodo p con todos los nodos de la lista (dinamico)
        while(j != nullptr) {
            //comparar los elementos (dato/nombre) de los nodos, si la condicion se cumple, se moveran las posiciones
            if(p->postre->get_postre() > j->postre->get_postre()){
                string aux = j->postre->get_postre();
                string aux2 = p->postre->get_postre();
                //reemplazo posiciones 
                j->postre->set_postre(aux2);
                p->postre->set_postre(aux); 
            }
            j = j->sgte;
        }
        p = p->sgte;
    }
}

/*

void ListaPostre::seleccion(string dato){
    // utiliza variable temporal para recorrer la lista. 
    NodoPostre *tmp = this->raiz;
    int opc,opc1;
    string line;

    // la recorre mientras sea distinto de NULL (no hay más nodos). 
    cout << "\n";
    while (tmp != NULL) {
        if(p->postre->get_postre() == dato){
            cout << "Ingrese opcion: ";
            cin >> opc;
                
            switch(opc){
                case 1:
                        lista_ingrediente->mostrar_ingrediente();
                break;

                case 2:
                    do{
                        cout << "Ingrese ingredientes del postre: ";
                        cin >> line;
                        p->postre->lista_ingrediente->agregar_ingrediente(new Ingrediente(line));

                        cout << "¿Continuar? (Si[1], No[2]): ";
                        cin >> opc1;

                    } while(opc1 != 2);
                    cout << "\n";
                    
                break;

                case 3:
                    cout << "Ingrese nombre del ingrediente que desee eliminar: ";
                    cin >> line;
                    lista_ingrediente->eliminar_ingrediente(line);
                    cout << "\nIngrediente " << line << " eliminado\n";
                    lista_ingrediente->mostrar_ingrediente();
                break;
        }
        tmp = tmp->sgte;
    }
}  
*/
