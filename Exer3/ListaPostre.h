
#ifndef LISTAPOSTRE_H
#define LISTAPOSTRE_H

//Librerias
#include <iostream>
using namespace std;

#include "Postre.h"
#include "Ingrediente.h"

/* define la estructura del nodo. */
typedef struct _NodoPostre {
    Postre *postre;
    struct _NodoPostre *sgte;
} NodoPostre;


class ListaPostre {
    private:
        NodoPostre *raiz = NULL;
        NodoPostre *ultimo = NULL;

    public:
        /* constructor*/
        ListaPostre();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Numero. */
        void agregar_postre(Postre *postre);
        /* imprime la lista. */
        void mostrar_postres();
        void eliminar_postre(string dato);
        void ordenar_postres();
        void seleccion(string dato);
};
#endif
