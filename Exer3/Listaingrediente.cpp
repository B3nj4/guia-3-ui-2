//Librerias
#include <iostream>
using namespace std;

#include "Listaingrediente.h"
#include "ListaPostre.h"
#include "Postre.h"
#include "Ingrediente.h"

Listaingrediente::Listaingrediente() {}

void Listaingrediente::agregar_ingrediente(Ingrediente *ingrediente) {
    Nodo *tmp;

    /* crea un nodo . */ 
    tmp = new Nodo;

    /* asigna la instancia de Numero. */
    tmp->ingrediente = ingrediente;
    
    /* apunta a NULL por defecto. */
    tmp->sgte = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo 
    como el último de la lista. */
    } else {
        this->ultimo->sgte = tmp;
        this->ultimo = tmp;
    }
}

void Listaingrediente::mostrar_ingrediente() {
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << "\n";
    while (tmp != NULL) {
        cout << "Ingrediente: " << tmp->ingrediente->get_ingrediente() << endl;
        tmp = tmp->sgte;
    }
}

void Listaingrediente::eliminar_ingrediente(string dato){
    Nodo *p = this->raiz;

    if(p->ingrediente->get_ingrediente() == dato) {
        raiz = raiz->sgte;
        delete(p);
    }
    
    else{
        while(p->ingrediente->get_ingrediente() != dato){
            p = p->sgte;
        }

        Nodo *j = this->raiz; 

        while(j->sgte != p){
            j = j->sgte;
        }
        
        j->sgte = p->sgte;

        p->sgte = NULL;
        delete(p);
    }
}

