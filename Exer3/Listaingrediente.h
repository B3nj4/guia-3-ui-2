#ifndef LISTAINGREDIENTE_H
#define LISTAINGREDIENTE_H

//Librerias
#include <iostream>
using namespace std;

#include "Postre.h"
#include "Ingrediente.h"

/* define la estructura del nodo. */
typedef struct _Nodo {
    Ingrediente *ingrediente;
    struct _Nodo *sgte;
} Nodo;


class Listaingrediente {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Listaingrediente();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Numero. */
        void agregar_ingrediente(Ingrediente *ingrediente);
        /* imprime la lista. */
        void mostrar_ingrediente();
        /* eliminar nodo */
        void eliminar_ingrediente(string dato);
};
#endif

