
//Librerias
#include "Postre.h"
#include "Listaingrediente.h"

//Construcctor de Nombre
Postre::Postre(string postre) {
    this->postre = postre;
} 

/*
Postre::Postre(string postre, Listaingrediente lista_ingrediente) {
    this->postre = postre;
    this->lista_ingrediente = lista_ingrediente;
} */

//Metodo (obtener nombre)
string Postre::get_postre() {
    return this->postre;
}

void Postre::set_postre(string postre){
    this->postre = postre;
}

