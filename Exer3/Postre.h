#ifndef POSTRE_H
#define POSTRE_H

//Librerias
#include <iostream>
using namespace std;

#include "Listaingrediente.h"

class Postre {
    private:
        string postre = "\0";
        //Listaingrediente lista_ingrediente;
    public:
        /* constructor */
        Postre(string postre);
        //Postre(string postre, Listaingrediente lista_ingrediente);
        /* métodos get */
        string get_postre();
        void set_postre(string postre);
};
#endif
