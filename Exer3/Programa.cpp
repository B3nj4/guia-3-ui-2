/*
Escriba un programa que permita mantener una lista ordenado alfabéticamente de postres. Cada postre,
debe mantener una lista de sus ingredientes (no necesariamente ordenada). Las acciones mı́nimas que
se requieren son:
Mostrar el listado de postres.
Agregar postre.
Eliminar postre.
Seleccionar postre para:
• Mostrar sus ingredientes.
• Agregar ingrediente.
• Eliminar ingrediente.
*/

//Librerias
#include <iostream>
using namespace std;

#include "Listaingrediente.h"
#include "ListaPostre.h"

// Menu
int menu(){
    string opc;

    cout << "-------------------------------" << endl;
    cout << "Mostrar listado de postres  [1]" << endl;
    cout << "Agregar postre              [2]" << endl;
    cout << "Eliminar postre             [3]" << endl;
    cout << "Seleccionar postre          [4]" << endl;
    cout << "Salir                       [0]" << endl;
    cout << "-------------------------------" << endl;
    cout << "Opcion: ";
    cin >> opc;

  return stoi(opc);
}

// Funcion principal del programa
int main(void) {

    // Objeto lista
    ListaPostre *lista_postre = new ListaPostre();
    Listaingrediente *lista_ingrediente = new Listaingrediente();

    //Variables
    int opc, opc2, opc3 = 0;
    string line, line2, line3;

    system("clear");

    //Interactuar con el menu
    do {
        opc = menu();

        switch (opc) {
            //Agregar nombre y mostrar informacion
            case 1:
                lista_postre->ordenar_postres();
                lista_postre->mostrar_postres();
                break;

            case 2:
                cout << "Ingrese nombre del postre: ";
                cin >> line;

                do{
                    cout << "Ingrese ingredientes del postre: ";
                    cin >> line2;
                    lista_ingrediente->agregar_ingrediente(new Ingrediente(line2));

                    cout << "¿Continuar? (Si[1], No[2]): ";
                    cin >> opc2;

                } while(opc2 != 2);
                cout << "\n";
                lista_postre->agregar_postre(new Postre(line));
                //lista_postre->agregar_postre(new Postre(line,lista_ingrediente));
                break;


            case 3:
                cout << "Ingrese nombre del postre que desee eliminar: ";
                cin >> line;
                lista_postre->eliminar_postre(line);
                cout << "\nPostre " << line << " eliminado\n";
                lista_postre->mostrar_postres();
                break;

            case 4:
                /*
                cout << "Ingrese postre: ";
                //lista_postre->mostrar_postres();
                cin >> line3;
                lista_postre->seleccion(line3); */
                break;
                
        }
    } while (opc != 0); // Sale del programa

    return 0;
}

