# Guia 3-UI-2

#Ejercicios Listas y Nodo

El proyecto consta de tres ejercicios donde se separan sublistas de una lista, se recopila informacion de strings dentro de listas y se crean listas para interactuar con postres e ingredientes de estos.

#Pre-requisitos

Para poder instalar los sofwares se necesita de un sistema operativo Linux como lo es Ubuntu (link de la instalacion https://ubuntu.com/download)

Es fundamental la instalacion del lenguaje C++ y sus componentes de compilacion en su version g++ 9.3.0 (vease el siguiente link https://hetpro-store.com/TUTORIALES/compilar-cpp-g-linux-en-terminal-leccion-1/)

Se necesitara la instalacion de un editor de texto/IDE como lo puede ser "Visual Studio Code". (vease el link para la instalacion https://ubunlog.com/visual-studio-code-editor-codigo-abierto-ubuntu-20-04/?utm_source=feedburner&utm_medium=%24%7Bfeed%2C+email%7D&utm_campaign=Feed%3A+%24%7BUbunlog%7D+%28%24%7BUbunlog%7D%29)

Por ultimo, para poder clonar el repositorio del sofware, es necesario tener instalado "Gitlab" correctamente.


#Comenzando  

Para poder acceder a este sofware se requerira de clonar el espacio de trabajo desde https://gitlab.com/B3nj4/guia-3-ui-2 usando la siguiente combinacion en su terminal Linux

    git clone https://gitlab.com/B3nj4/guia-3-ui-2

Luego, se debera ingresar por los archivos correspondiente de cada ejercicio mediante terminal

    cd Exer1

    cd Exer2

    cd Exer3

Ya dentro de la carpeta del ejercicio que se desea visualizar, se requiere compilar el programa, para ello hay que ejecutar el "Makefile" escribiendo la siguiente terminacion en la terminal (debera hacerlo para cada carpeta de ejercicio en la que se encuentre)

    make

Ya ejecutado el "Makefile" debera ejecutar el sofware, para ello es fundamental ingresar lo siguiente en su terminal Linux (debera hacerlo para cada carpeta de ejercicio en la que se encuentre)

    ./Programa



Si se ha seguido todos los pasos, podra interactuar con el sofware sin ningún problema

     
#Construido con

Para la creacion de este proyecto se necesito del editor de texto Visual Studio Code y el lenguaje de programacion "C++" 


#Autor
    
    Benjamin Vera Garrido - Ingenieria Civil en Bioinformatica

